<?php

/**
 * View Helper Message
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Zend_View_Helper_Messages extends Zend_View_Helper_Abstract
{
    public function messages()
    {
        $messenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $messages = $messenger->getMessages();

        $response = '';
        foreach ($messages as $message) {
            if ($message) {
                $response .= $this->showMessage($message) . PHP_EOL;
            }
        }

        return $response;
    }

    public function showMessage($text)
    {
        $html =
            '<div class="alert">' .
            '<button type="button" class="close" data-dismiss="alert">&times;</button>' .
            $text .
            '</div>';

        return $html;
    }
}
