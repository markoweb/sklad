<?php

/**
 * IndexController
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class IndexController extends Zend_Controller_Action
{
    /**
     * @var Application_Model_Db_Product
     * Product Controller
     */
    protected $_modelProduct;

    public function init()
    {
        $this->_modelProduct = new Application_Model_Db_Product();
        $this->view->messages = 'Message';
    }

    public function indexAction()
    {
        $modelProduct = $this->_modelProduct;
        $result = $modelProduct->fetchEntries();
        $this->view->items = $result;
    }

    public function createAction()
    {
        $modelProduct = $this->_modelProduct;
        $form = new Application_Form_ProductCreate();

        if ($this->getRequest()->isPost() &&
            $this->getRequest()->getPost('send_product', false) !== false) {

            if ($form->isValid($this->getRequest()->getPost())) {

                $rowProduct = $modelProduct->createRow(
                        $form->getValues());

                if ($rowProduct->save()) {
                    $this->_helper->FlashMessenger('Zaznam pridany');
                }

                $this->_redirect('/');
            }
        }

        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');

        $modelProduct = $this->_modelProduct;
        $rowProduct = $modelProduct->find($id)->current();
        if (!$rowProduct) {
            throw new Exception('Wrong ID');
        }
        $productId = $rowProduct->id;

        $form = new Application_Form_ProductCreate();
        $form->setAction('/edit/' . $productId);

        if ($this->getRequest()->isPost() &&
            $this->getRequest()->getPost('send_product', false) !== false) {

            if ($form->isValid($this->getRequest()->getPost())) {

                $rowProduct->setFromArray($form->getValues());

                if ($rowProduct->save()) {
                    $this->_helper->FlashMessenger('Udaje boli zmenene');
                }

                $this->_redirect('/');
            }
        } else {
            $form->populate($rowProduct->toArray());
        }

        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        $modelProduct = $this->_modelProduct;
        $rowProduct = $modelProduct->find($id)->current();
        if (!$rowProduct) {
            throw new Exception('Wrong ID');
        }
        $productId = $rowProduct->id;

        $form = new Application_Form_ProductDelete();
        $form->setAction('/delete/' . $productId);

         if ($this->getRequest()->isPost() &&
             $this->getRequest()->getPost('del_product', false) !== false) {

             if ($form->isValid($this->getRequest()->getPost())) {

                 $where = $modelProduct->getAdapter()->quoteInto('id = ?', $productId);
                 $modelProduct->delete($where);
                 $this->_helper->FlashMessenger('Zaznam bol vymazany');
                 $this->_redirect('/');
             }
         } else {

             $form->populate($rowProduct->toArray());
         }

         $this->view->item  = $rowProduct;
         $this->view->form  = $form;
    }
}
