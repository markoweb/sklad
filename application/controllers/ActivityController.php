<?php

/**
 * ActivityController
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class ActivityController extends Zend_Controller_Action
{
    /**
     * Model Product
     * @var Zend_Db_Table_Row
     */
    protected $_product;

    /**
     * Product Id
     * @var int
     */
    protected $_prodId;


    public function init()
    {
        $id = $this->getRequest()->getParam('ida');
        $this->_prodId = $id ? $id : null;
    }

    public function indexAction()
    {
        $modelItems = new Application_Model_Db_Product();
        $this->_product = $modelItems->find($this->_prodId)->current();

        if (!$this->_product) {
            throw new Exception('Wrong ID');
//            $this->_helper->FlashMessenger('Nespravne ID produktu');
//            $this->_redirect('/');
        }
        $this->_prodId = (int) $this->_product->id;

        $modProd = $this->_product;
        $modAct = $modProd->findDependentRowset(
                new Application_Model_Db_Activity());

        // Suppliers
        $modSupp = new Application_Model_Db_Supplier();
        $allSuppliers = $modSupp->fetchAll();
        foreach ($allSuppliers as $supplier) {
            $suppliers[$supplier['id']] = $supplier['name'];
        }

        $this->view->ida    = $this->_prodId;
        $this->view->prod   = $modAct;
        $this->view->supp   = $suppliers;
    }

    public function createAction()
    {
        $modActivity = new Application_Model_Db_Activity();
        $form = new Application_Form_ActivityCreate();
        $form->setAction('/activity/create');

        if ($this->getRequest()->isPost() &&
            $this->getRequest()->getPost('send_activity', false) !== false) {

            if ($form->isValid($this->getRequest()->getPost())) {

                $rowActivity = $modActivity->createRow($form->getValues());

                if ($rowActivity->save()) {
                    $this->_helper->FlashMessenger('Zaznam pridany');
                }

                $this->_redirect('/activity/' . $rowActivity->product_id);
            }

        } else {

            $form->populate(array('product_id' => $this->_prodId));
        }

        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');

        $modelActivity = new Application_Model_Db_Activity();
        $rowActivity = $modelActivity->find($id)->current();

        if (!$rowActivity) {
            throw new Exception('Wrong ID');
        }

        $activityId = $rowActivity->id;

        $form = new Application_Form_ActivityDelete();
        $form->setAction('/activity/' . $this->_prodId . '/delete/' . $activityId);

         if ($this->getRequest()->isPost() &&
             $this->getRequest()->getPost('del_activity', false) !== false) {

             if ($form->isValid($this->getRequest()->getPost())) {

                 $where = $modelActivity->getAdapter()->quoteInto('id = ?', $activityId);
                 $modelActivity->delete($where);
                 $this->_helper->FlashMessenger('Zaznam bol vymazany');
                 $this->_redirect('/');
             }
         } else {

             $form->populate($rowActivity->toArray());
         }

         $this->view->item  = $rowActivity;
         $this->view->form  = $form;
    }
}
