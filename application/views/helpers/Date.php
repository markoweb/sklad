<?php

/**
 * View Helper Date
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Zend_View_Helper_Date extends Zend_View_Helper_Abstract
{
    public function date($strDate)
    {
        $timestamp = strtotime($strDate);
        $response = date('d.m.Y', $timestamp);

        return $response;
    }
}
