<?php

/**
 * ActivityCreate
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Form_ActivityCreate extends Zend_Form
{
    public function init()
    {
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setAttrib('id', 'activity_create');

        $modActivity    = new Application_Model_Db_Activity();
        $arrProducts    = $modActivity->fetchProducts();
        $arrSuppliers   = $modActivity->fetchSuppliers();

        $actProduct = new Zend_Form_Element_Select('product_id', array(
            'required' => true, 'multiOptions' => $arrProducts,
            'label' => 'Produkt',
        ));

        $actSupplier = new Zend_Form_Element_Select('supplier_id', array(
            'required' => true, 'multiOptions' => $arrSuppliers,
            'label' => 'Dodavatel',
        ));

        $actQuantity = new Zend_Form_Element_Text('quantity', array(
            'label' => 'Mnozstvo',
            'required' => true,
            'validators' => array('Digits'),
        ));

        $actSubmit = new Zend_Form_Element_Submit('send_activity', array(
            'label' => 'Pridat', 'class' => 'btn',
        ));

        $this->addElements(array(
            $actProduct, $actSupplier, $actQuantity, $actSubmit));
    }
}
