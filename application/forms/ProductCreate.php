<?php

/**
 * ProductCreate
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Form_ProductCreate extends Zend_Form
{
    public function init()
    {
        $this->setAction('/create');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setAttrib('id', 'product_create');

        $productName = new Zend_Form_Element_Text('name', array(
            'label' => 'Nazov produktu',
            'required' => true,
            'validators' => array('Alnum'),
        ));

        $productSend = new Zend_Form_Element_Submit('send_product', array(
            'label' => 'ulozit',
            'class' => 'btn',
        ));

        $this->addElements(array(
            $productName, $productSend
        ));
    }
}
