<?php

/**
 * ActivityDelete
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Form_ActivityDelete  extends Zend_Form
{
    public function init()
    {
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setAttrib('id', 'activity_delete');

        $activityId = new Zend_Form_Element_Hidden('id', array(
            'required' => true
        ));

        $submitDelete = new Zend_Form_Element_Submit('del_activity', array(
            'label' => 'Vymazat',
            'class' => 'btn'
        ));

        $this->addElements(array(
            $activityId, $submitDelete
        ));
    }
}
