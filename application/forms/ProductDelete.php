<?php

/**
 * ProductDelete
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Form_ProductDelete  extends Zend_Form
{
    public function init()
    {
        $this->setAction('/delete');
        $this->setMethod(Zend_Form::METHOD_POST);
        $this->setAttrib('id', 'product_delete');

        $productId = new Zend_Form_Element_Hidden('id', array(
            'required' => true
        ));

        $submitDelete = new Zend_Form_Element_Submit('del_product', array(
            'label' => 'Vymazat',
            'class' => 'btn'
        ));

        $this->addElements(array(
            $productId, $submitDelete
        ));
    }
}
