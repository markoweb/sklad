<?php

/**
 * Model Db Activity
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Model_Db_Activity extends Zend_Db_Table_Abstract
{
    protected $_name = 'activity';
    protected $_primary = 'id';

    protected $_referenceMap = array(

        'Application_Model_Db_Product' => array(
            'columns'       => 'product_id',
            'refTableClass' => 'Application_Model_Db_Product',
            'refColums'     => 'id',
        ),
        'Application_Model_Db_Supplier' => array(
            'columns'       => 'supplier_id',
            'refTableClass' => 'Application_Model_Db_Supplier',
            'refColumns'    => 'id'
        ),
    );

    public function fetchProducts()
    {
        $modProd = new Application_Model_Db_Product();
        return $this->getAdapter()->fetchPairs($modProd->select());
    }

    public function fetchSuppliers()
    {
        $modSupp = new Application_Model_Db_Supplier();
        return $this->getAdapter()->fetchPairs($modSupp->select());
    }
}
