<?php

/**
 * Model Db Products
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Model_Db_Product extends Zend_Db_Table_Abstract
{
    protected $_name = 'product';
    protected $_primary = 'id';

    public function fetchEntries()
    {
        $db = $this->getAdapter();

        $select = $db->select()
                ->from(array('p' => 'product'), array('p.id', 'p.name'))
                ->joinLeft( array( 'a' => 'activity' ), 'p.id = a.product_id',
                            array('sum' => new Zend_Db_Expr('SUM(a.quantity)')))
                ->group('p.id')
                ->order('p.name');
        $data = $db->fetchAll($select);

        return $data;
    }
}
