<?php

/**
 * Model Db Suppliers
 *
 * @package     Sklad
 * @author      Matus Marko <matus@markoweb.sk>
 */
class Application_Model_Db_Supplier extends Zend_Db_Table_Abstract
{
    protected $_name = 'supplier';
    protected $_primary = 'id';
}
