<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initRoutes()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $route = new Zend_Controller_Router_Route(
                ':action/:id',
                array(
                    'module'        => 'default',
                    'controller'    => 'index',
                    'id'            => null,
                ),
                array(
                    'action' => '(index|create|edit|delete)',
                    'id'     => '\d+',
                )
        );
        $router->addRoute('products', $route);

        $route = new Zend_Controller_Router_Route(
                'activity/:ida/:action/:id',
                array(
                    'module'        => 'default',
                    'controller'    => 'activity',
                    'action'        => 'index',
                    'id'            => null,
                ),
                array(
                    'action'    => '(index|create|delete)',
                    'ida'       => '\d+',
                    'id'        => '\d+',
                )
        );
        $router->addRoute('activity', $route);
    }
}

